'use strict';

// The module 'vscode' contains the VS Code extensibility API.
// 'open' has the functionality for opening a URL.
// 'git' contains the git functionality for navigating to source or to builds.
import * as vscode from 'vscode';
import * as git from './git_source';
import * as fs from 'fs';
import { open } from './open';
import { join } from 'path';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    console.log('Entered Activate() for extension "bitbucket-pipelines-deploy-to-azure".');

    // The commands have been defined in the package.json file.
    // Provided below are the implementations of the commands with registerCommand.
    // The commandId parameter must match the command field in package.json.
    let disposableGoToSource = vscode.commands.registerCommand('extension.goToSource', () => {
        git.openSourceInBitbucket();
    });
    
    let disposableGoToPieplines = vscode.commands.registerCommand('extension.goToPipelines', () => {
        // Example URL: https://bitbucket.org/user1234/test-repo123/addon/pipelines/home#!/
        let pipelinesUrlSuffix:string = '/addon/pipelines/home#!/';
        git.getBitbucketUrl(pipelinesUrlSuffix);
    });

    let disposableAddPipelinesConfigFile = vscode.commands.registerCommand('extension.addPipelinesConfigFile', () => {

        let cwd:string = vscode.workspace.rootPath;
        
        if (!cwd) {
            vscode.window.showWarningMessage('Please select a working folder for your workspace first.');
            return;
        }
        
        // 1) Add 'bitbucket-pipelines.yml' to the workspace:
        addFileToWorkspace(cwd, 'bitbucket-pipelines.yml', getPipelinesConfigFile);
                        
        // 2) Add 'deploy-to-azure.bash' to the workspace.
        addFileToWorkspace(cwd, 'deploy-to-azure.bash', getPipelinesScriptFile);
        
        // 3) Add 'deploy-to-azure.md' to the workspace.
        addFileToWorkspace(cwd, 'deploy-to-azure.md', getPipelinesDescriptionFile);
    });

    let disposableAddPipelinesConfigFileKudu = vscode.commands.registerCommand('extension.addPipelinesConfigFileKudu', () => {

        let cwd:string = vscode.workspace.rootPath;
        
        if (!cwd) {
            vscode.window.showWarningMessage('Please select a working folder for your workspace first.');
            return;
        }
        
        // 1) Add 'bitbucket-pipelines.yml' to the workspace:
        addFileToWorkspace(cwd, 'bitbucket-pipelines.yml', getPipelinesConfigFileKudu);
                        
        // 2) Add 'deploy-to-azure.bash' to the workspace.
        addFileToWorkspace(cwd, 'deploy-to-azure.bash', getPipelinesScriptFileKudu);
        
        // 3) Add 'deploy-to-azure.md' to the workspace.
        addFileToWorkspace(cwd, 'deploy-to-azure.md', getPipelinesDescriptionFileKudu);
    });

    let disposableTryAzureAppService = vscode.commands.registerCommand('extension.tryAzureAppService', () => {
        open('https://tryappservice.azure.com/', '', null);
    });

    let disposableTryDeployToAzurePlugin = vscode.commands.registerCommand('extension.tryDeployToAzurePlugin', () => {
        open('https://marketplace.atlassian.com/plugins/deploy-to-azure/cloud/overview', '', null);
    });

    context.subscriptions.push(disposableGoToSource);
    context.subscriptions.push(disposableGoToPieplines);
    context.subscriptions.push(disposableAddPipelinesConfigFile);
    context.subscriptions.push(disposableAddPipelinesConfigFileKudu);
    context.subscriptions.push(disposableTryAzureAppService);
    context.subscriptions.push(disposableTryDeployToAzurePlugin);
}

function addFileToWorkspace(cwd:string, filename:string, getContentCallback:any) {
    let pipelinesConfigUri = join(cwd + '/' + filename);

    let docContent = getContentCallback();
    fs.writeFile(pipelinesConfigUri, docContent, function(err) {
        if(err) {
            return console.log('Could not write \'' + filename + '\': ' + err);
        }

        vscode.window.showInformationMessage('Successfully added ' + filename + ' to workspace!');
    });
    
    return;
}

function getPipelinesConfigFile() {
    return  'pipelines:\n' +
            '  default:\n' +
            '    - step:\n' +
            '        script:  # Modify the commands below to build and test your repository.\n' +
            '          - apt-get update\n' +
            '          - apt-get install ncftp\n' +
            '          - chmod +x ./deploy-to-azure.bash\n' +
            '          - ./deploy-to-azure.bash\n';
} 

function getPipelinesScriptFile() {
    return  '#!/bin/bash -ex\n' +
            '\n' +
            '# These variables should be defined in the Builds environment variables.\n' +
            '# Retrieve them from an Azure Web App\'s Publishing Profile (.publishsettings).\n' +
            '# - FTP_HOST (e.g.: waws-prod-bay-039.ftp.azurewebsites.windows.net)\n' +
            '# - FTP_USERNAME (e.g.: webappname\$webappname)\n' +
            '# - FTP_PASSWORD\n' +
            '# - FTP_SITE_ROOT (e.g.: /site/wwwroot)\n' +
            '\n' +
            '# Upload all files recursively from local directory to the remote directory under the Azure Web App:\n' +
            '# NOTE: If the FTP_PASSWORD environment variable was added with the "Secured" checkbox checked,\n' +
            '#       then the password will not be shown in cleartext in the build logs, and will instead be replaced\n' +
            '#       with the "$FTP_PASSWORD" token. Another cool feature of Bitbucket Pipelines.\n' +
            '\n' +
            '# 1) Upload all files:\n' +
            'ncftpput -v -u "$FTP_USERNAME" -p "$FTP_PASSWORD" -R $FTP_HOST $FTP_SITE_ROOT *\n' +
            '\n' +
            '# 2) Upload all folders recursively (except hidden folders like .git):\n' +
            'ncftpput -v -u "$FTP_USERNAME" -p "$FTP_PASSWORD" -R $FTP_HOST $FTP_SITE_ROOT */\n' +
            '\n' +
            'echo Finished uploading files to $FTP_HOST$FTP_SITE_ROOT.';
}

function getPipelinesDescriptionFile() {
    return  '# Bitbucket Pipelines Deploy-to-Azure Extension #\n' +
            'This document describes the process of publishing to Azure Web App using FTP.\n' +
            '\n' +
            'The following files were added by the \'bitbucket-pipelines-deploy-to-azure\' for VS Code:\n' +
            '* bitbucket-pipelines.yml\n' +
            '* deploy-to-azure.bash\n' +
            '* deploy-to-azure.md (this file)\n' +
            '\n' +
            '### How to use the generated artifacts: ###\n' +
            '\n' +
            '* First, create a new repository and create a new web app.\n' +
            '* Next, enable Pipelines for your repository and add the default **`bitbucket-pipelines.yml`** file to your repo.\n' +
            '* Create a [Web App](https://azure.microsoft.com/en-us/documentation/articles/app-service-web-how-to-create-a-web-app-in-an-ase/) in the [Azure Portal](https://portal.azure.com).\n' +
            '* Download the **Publish Profile** (*your-web-app-name*.PublishSettings*) file from the Web App blade in the [Azure Portal](https://portal.azure.com).\n' +
            '* Set the following environment variables in your Bitbucket repo (retrieve their values from the .PublishSettings file\'s PublishProfile with the profileName \'your-web-app-name - FTP\'):\n' +
            '\n' +
            '1.  **`FTP_HOST`** (e.g.: *waws-prod-bay-039.ftp.azurewebsites.windows.net*)\n' +
            '2.  **`FTP_USERNAME`** (e.g.: _webappname\$webappname_)\n' +
            '3.  **`FTP_PASSWORD`** (*IMPORTANT NOTE*: Make sure the "**Secured**" checkbox is checked!)\n' +
            '4.  **`FTP_SITE_ROOT`** (e.g.: _/site/wwwroot_)\n' +
            '\n' +
            '* Use this extension to add a **`bitbucket-pipelines.yml`** file and a **`deploy-to-azure.bash`** file that are ready for deploying to Azure App Service.\n' +
            '* Push a change to your repo and watch Pipelines validate and then publish your changes to your Azure Web App!\n';
}

function getPipelinesConfigFileKudu() {
    return  'pipelines:\n' +
            '  default:\n' +
            '    - step:\n' +
            '        script:  # Modify the commands below to build and test your repository.\n' +
            '          - chmod +x ./deploy-to-azure.bash\n' +
            '          - ./deploy-to-azure.bash\n';
} 

function getPipelinesScriptFileKudu() {
    return  '#!/bin/bash -ex\n' +
            '\n' +
            '# This script utilizes a less secure way of triggering deployment to Azure Web Apps.\n' +
            '# - It calls the Kudu service and provides it with the Git account credendials so that\n' +
            '# - Kudu can pull source files from the specified repository. This operation is \n' +
            '#   synchronous (blocks until either the \'fetch\' deployment completes successfully or errors out).\n' +
            '# NOTE: This approach is not recommended for production deployments as it is not very secure.\n' +
            '#       It is only provided here as an example of the different deployment options available.\n' +
            '#\n' +
            '# These variables should be defined in the Builds environment variables.\n' +
            '# Retrieve them from an Azure Web App\'s Publishing Profile (.publishsettings).\n' +
            '# - FTP_PASSWORD\n' +
            '# - SITE_NAME (e.g.: webappname)\n' +
            '#\n' +
            '# In addition, the following environment variable for the Bitbucket account are needed:\n' +
            '# - BITBUCKET_USERNAME\n' +
            '# - BITBUCKET_PASSWORD (check the \'Secured\' flag)\n' +
            '# - REPOSITORY_NAME (e.g. my-code-repo)\n' +
            '\n' +
            '# Trigger the Kudu service of the Azure Web App:\n' +
            '# NOTE: If the FTP_PASSWORD and BITBUCKET_PASSWORD environment variables were added with the "Secured" checkbox checked,\n' +
            '#       then the passwords will not be shown in cleartext in the build logs, and will instead be replaced\n' +
            '#       with the "$FTP_PASSWORD" and "BITBUCKET_PASSWORD" tokens. Another cool feature of Bitbucket Pipelines.\n' +
            '\n' +
            '# Trigger a manual (fetch) Kudu deployemnt:\n' +
            'curl -X POST "https://\\$$SITE_NAME:$FTP_PASSWORD@$SITE_NAME.scm.azurewebsites.net/deploy" \\\n' +
            '  --header "Content-Type: application/json" \\\n' +
            '  --header "Accept: application/json" \\\n' +
            '  --header "X-SITE-DEPLOYMENT-ID: $SITE_NAME" \\\n' +
            '  --header "Transfer-encoding: chunked" \\\n' +
            '  --data "{\\"format\\":\\"basic\\", \\"url\\":\\"https://$BITBUCKET_USERNAME:$BITBUCKET_PASSWORD@bitbucket.org/$BITBUCKET_USERNAME/$REPOSITORY_NAME.git\\"}"\n' +
            '\n' +
            'echo Finished uploading files to site $SITE_NAME.\n';
}

function getPipelinesDescriptionFileKudu() {
    return  '# Bitbucket Pipelines Deploy-to-Azure Extension #\n' +
            'This document describes the process of publishing to Azure Web App using the manual (fetch) Kudu deployment feature.\n' +
            '\n' +
            'The following files were added by the \'bitbucket-pipelines-deploy-to-azure\' for VS Code:\n' +
            '* bitbucket-pipelines.yml\n' +
            '* deploy-to-azure.bash\n' +
            '* deploy-to-azure.md (this file)\n' +
            '\n' +
            '### How to use the generated artifacts: ###\n' +
            '\n' +
            '* First, create a new repository and create a new web app.\n' +
            '* Next, enable Pipelines for your repository and add the default **`bitbucket-pipelines.yml`** file to your repo.\n' +
            '* Create a [Web App](https://azure.microsoft.com/en-us/documentation/articles/app-service-web-how-to-create-a-web-app-in-an-ase/) in the [Azure Portal](https://portal.azure.com).\n' +
            '* Download the **Publish Profile** (*your-web-app-name*.PublishSettings*) file from the Web App blade in the [Azure Portal](https://portal.azure.com).\n' +
            '* Set the following environment variables in your Bitbucket repo (retrieve their values from the .PublishSettings file\'s PublishProfile with the profileName \'your-web-app-name - FTP\'):\n' +
            '\n' +
            '1.  **`FTP_PASSWORD`** (*IMPORTANT NOTE*: Make sure the "**Secured**" checkbox is checked!)\n' +
            '2.  **`SITE_NAME`** (e.g.: _webappname_)\n' +
            '3.  **`BITBUCKET_USERNAME`**\n' +
            '4.  **`BITBUCKET_PASSWORD`** (*IMPORTANT NOTE*: Make sure the "**Secured**" checkbox is checked!)\n' +
            '5.  **`REPOSITORY_NAME`** (e.g. my-code-repo)\n' +
            '\n' +
            '* Use this extension to add a **`bitbucket-pipelines.yml`** file and a **`deploy-to-azure.bash`** file that are ready for deploying to Azure App Service.\n' +
            '* Push a change to your repo and watch Pipelines validate and then publish your changes to your Azure Web App!\n';
}

// this method is called when your extension is deactivated
export function deactivate() {
}